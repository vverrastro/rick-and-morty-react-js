import React, { Component } from 'react';

// Styles
import '../styles/Menu.css';

// The Menu component displays the list of items menu
class Menu extends Component {

    render() {

        let itemsMenu;

        if (this.props.itemsMenu) {
            itemsMenu = this.props.itemsMenu.map(item =>(
                <li key={item.name}><a rel="noopener noreferrer" target="_blank" href={item.link}>{item.name}</a></li>
            ));
        }

        return (
            <nav>
                <ul>
                    {itemsMenu}
                </ul>
            </nav>
        );
    }
}

export default Menu;