import React, { Component, Fragment } from 'react';

// Styles
import '../styles/Error.css';
import logoNotFound from '../404.png';

// Configuration
import exceptions from "../exceptions.json";

// The Error component represents the 'box' that displays a message error or the 404 image in case of notFound error type
class Error extends Component {

	render() {
		return (
			<Fragment>
				{(this.props.type && this.props.type === exceptions.notFound) ? (
					<div className="Error-container">
					<img src={logoNotFound} alt={exceptions.notFound} />  
					</div>
				) : (
					<div className="Error">
					Error: {this.props.message}
					</div>
				)}
			</Fragment>
		);
	}
}

export default Error;