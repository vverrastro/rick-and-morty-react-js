import React, { Component } from 'react';

// Configuration
import configData from "../config.json";

// Styles
import '../styles/Footer.css';

// The Footer component displays information about the author and the project name
class Footer extends Component {

render() {
    return (
        <div className='Footer'>
            <p>The Rick and Morty Front End React Project | 2021 | <a rel="noopener noreferrer" target="_blank" href={configData.author.link}>{configData.author.name}</a></p>
        </div>
    );
  }
}

export default Footer;