import React, { Component } from 'react';

// Components
import Location from './Location';
import EpisodeDropdown from './EpisodeDropdown';

// Styles
import '../styles/Card.css';

// The Card component represents the single card in which character information are displayed
class Card extends Component {

    render() {

        // Setup character icon status checking the character status property (dead/alive/unknown)
        // The className status will be assigned to the span near Status Property in order to show a specific color
        let statusClassname = "icon-status-unknown";

        if (this.props.character.status && this.props.character.status.toLowerCase() === 'dead') {
            statusClassname = "icon-status-dead";
        } else if (this.props.character.status && this.props.character.status.toLowerCase() === 'alive') {
            statusClassname = "icon-status-alive";
        }

        // Setup origin and current location information/properties
        let originUrl, currentLocationUrl;
        const labelOrigin = "Origin";
        const labelLocation = "Current location";

        if (this.props.character.origin && this.props.character.origin.url) {
            originUrl = this.props.character.origin.url;
        }

        if (this.props.character.location && this.props.character.location.url) {
            currentLocationUrl = this.props.character.location.url;
        }

        return (
            <div className="Card">
                <div className="Card-avatar">
                    <img src={this.props.character.image} alt={this.props.character.name} />
                </div>
                <div className="Card-corpus">
                    <div className="section-text">
                        <h2>{this.props.character.name}</h2>
                        <span className="status"><span className={statusClassname}></span>{this.props.character.status} - {this.props.character.species} - {this.props.character.gender}</span>
                    </div>
                    <div className="section-text">
                        <p className="secondary-text">{labelOrigin}</p>
                        {originUrl ? (
                            <Location link={this.props.character.origin.url} />
                        ) : (
                            <span>{this.props.character.origin.name}</span>
                        )}
                    </div>
                    <div className="section-text">
                        <p className="secondary-text">{labelLocation}</p>
                        {currentLocationUrl ? (
                            <Location link={this.props.character.location.url} />
                        ) : (
                            <span>{this.props.character.location.name}</span>
                        )}
                    </div>
                    <div className="section-text">
                        <EpisodeDropdown episodesLink={this.props.character.episode}/>
                    </div>
                </div>
            </div>
        );
    }
}

export default Card;