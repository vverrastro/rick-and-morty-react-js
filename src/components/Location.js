import React, { Component, Fragment } from 'react';

// Components
import Error from './Error';

// The Location component displays information origin and current location
class Location extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            location: null
        }
    }

    // Rickandmorty location request
    makeHttpLocationRequest = link => {
    fetch(`${link}`, {
    method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    })
    .then(res => res.json())
    .then(
        (data) => {
            this.setState({ location: data });
        },
        (error) => {
            this.setState({
                error
            });
        }
    )
    }

    componentDidMount() {
        this.makeHttpLocationRequest(this.props.link);
    }

    render() {

        const { error, location } = this.state;

        if (error) {
            return <Error message={error.message}/>
        } else {

            let name, dimension, type, residentLabel;

            if (location) {
                name = location.name;
                dimension = location.dimension;
                type = location.type;
    
                let residents = location.residents.length;
                
                if (residents === 1) {
                    residentLabel = `${residents} resident`;
                } else {
                    residentLabel = `${residents} residents`;
                }
            }

            return (
                <Fragment>
                    <span>{type} | {name} | {dimension}</span>
                    <span>{residentLabel} </span>
                </Fragment>
            );
        }
    }
}

export default Location;