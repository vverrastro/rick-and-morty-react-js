import React, { Component } from 'react';

// Components
import Error from './Error';

// Styles
import '../styles/EpisodeDropdown.css';

// The EpisodeDropdown component represents the dropdown that displays the chapters the character is featured on
class EpisodeDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
            error: null,
            episodesLink: [],
            episodes: []
        }
    }

    // Rickandmorty episode request
    makeHttpEpisodeRequest () {

    this.setState({ episodes: [] });

    this.state.episodesLink.forEach(link => {
        fetch(`${link}`, {
        method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(
            (data) => {
                this.setState({ episodes: this.state.episodes.concat(data.name) });
            },
            (error) => {
                this.setState({
                    error
                });
            }
        )
    });
    }

    componentDidMount() {
        this.setState({
            episodesLink: this.props.episodesLink
        });
    }

    render() {

        const { error } = this.state;

        if (error) {
            return <Error message={error.message}/>
        } else {

            const labelEpisode = "Chapters the character is featured on";

            return (
                <div className="EpisodeDropdown">
                    <button className="EpisodeDropdown-button" onClick={() => this.makeHttpEpisodeRequest()}> {labelEpisode} </button>
                    <div className="EpisodeDropdown-content">
                        {this.state.episodes.map(function(episode){
                            return <span key={episode}>{episode}</span>;
                        })}
                    </div>
                </div> 
            );
         }
    }
}

export default EpisodeDropdown;