import { render, screen, waitFor } from '@testing-library/react';

import { rest } from 'msw'
import { setupServer } from 'msw/node'

import Location from '../components/Location';
import locationMock from "./mocks/location.json"; 

const link = 'https://rickandmortyapi.com/api/location/1';

const server = setupServer(
    rest.get(link, (req, res, ctx) => {
        // respond using a mocked JSON body
        return res(ctx.json(locationMock))
    })
)

// establish API mocking before all tests
beforeAll(() => server.listen())
// reset any request handlers that are declared as a part of our tests
// (i.e. for testing one-time error scenarios)
afterEach(() => server.resetHandlers())
// clean up once the tests are done
afterAll(() => server.close())

test('displays location information', async () => {
    render(<Location link={link}/>);

    await waitFor(() => expect(screen.getByText(locationMock.name, { exact: false })).toBeInTheDocument());
    await waitFor(() => expect(screen.getByText(locationMock.type, { exact: false })).toBeInTheDocument());
    await waitFor(() => expect(screen.getByText(locationMock.dimension, { exact: false })).toBeInTheDocument());
    await waitFor(() => expect(screen.getByText(/resident/i, { exact: false })).toBeInTheDocument());
})