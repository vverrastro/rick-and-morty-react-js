import { render, screen, waitFor } from '@testing-library/react';

import Card from '../components/Card';
import characterMock from "./mocks/character.json"; 

test('displays character information', async () => {
    render(<Card character={characterMock.results[0]} />);

    await waitFor(() => expect(screen.getByText(characterMock.results[0].name)).toBeInTheDocument());

    expect(screen.getByRole('img')).toHaveAttribute('src', characterMock.results[0].image);

    expect(screen.getByText(characterMock.results[0].status, { exact: false })).toBeInTheDocument();
    expect(screen.getByText(characterMock.results[0].species, { exact: false })).toBeInTheDocument();
    expect(screen.getByText(characterMock.results[0].gender, { exact: false })).toBeInTheDocument();
})