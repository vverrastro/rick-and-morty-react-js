import { render, screen, waitFor } from '@testing-library/react';

import { rest } from 'msw'
import { setupServer } from 'msw/node'

import App from '../App';
import characterMock from "./mocks/character.json"; 

const link = 'https://rickandmortyapi.com/api/character';

const server = setupServer(
    rest.get(link, (req, res, ctx) => {
        // respond using a mocked JSON body
        return res(ctx.json(characterMock))
    })
)

// establish API mocking before all tests
beforeAll(() => server.listen())
// reset any request handlers that are declared as a part of our tests
// (i.e. for testing one-time error scenarios)
afterEach(() => server.resetHandlers())
// clean up once the tests are done
afterAll(() => server.close())

test('handles server error', async () => {
	server.use(
		rest.get(link, (req, res, ctx) => {
			return res(ctx.status(500))
		})
	)
	render(<App />);
	await waitFor(() => expect(screen.getByRole('img')).toHaveAttribute('src', '404.png'));
})

test('displays characters information', async () => {
	render(<App />);

	await waitFor(() => expect(screen.getByText(characterMock.results[0].name, { exact: false })).toBeInTheDocument());

	expect(screen.getAllByText(characterMock.results[0].status, { exact: false })[0]).toBeInTheDocument();
	expect(screen.getAllByText(characterMock.results[0].species, { exact: false })[0]).toBeInTheDocument();
	expect(screen.getAllByText(characterMock.results[0].gender, { exact: false })[0]).toBeInTheDocument();

	expect(screen.getByText(characterMock.results[19].name, { exact: false })).toBeInTheDocument();
	expect(screen.getAllByText(characterMock.results[19].status, { exact: false })[0]).toBeInTheDocument();
	expect(screen.getAllByText(characterMock.results[19].species, { exact: false })[0]).toBeInTheDocument();
	expect(screen.getAllByText(characterMock.results[19].gender, { exact: false })[0]).toBeInTheDocument();
})

test('displays pagination', async () => {
	render(<App />);
	await waitFor(() => expect(screen.getByText("1", { exact: true })).toBeInTheDocument());
	await waitFor(() => expect(screen.getByText("2", { exact: true })).toBeInTheDocument());
})
  
test('verify pagination length', async () => {
	render(<App />);
	await waitFor(() => expect(screen.getByText(characterMock.info.pages, { exact: true })).toBeInTheDocument());
})