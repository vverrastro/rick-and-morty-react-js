import { render, screen, waitFor, fireEvent } from '@testing-library/react';

import { rest } from 'msw'
import { setupServer } from 'msw/node'

import EpisodeDropdown from '../components/EpisodeDropdown';
import characterMock from "./mocks/character.json"; 
import episodeMock from "./mocks/episode.json"; 

const link = 'https://rickandmortyapi.com/api/episode/1';

const server = setupServer(
    rest.get(link, (req, res, ctx) => {
        // respond using a mocked JSON body
        return res(ctx.json(episodeMock))
    })
)

// establish API mocking before all tests
beforeAll(() => server.listen())
// reset any request handlers that are declared as a part of our tests
// (i.e. for testing one-time error scenarios)
afterEach(() => server.resetHandlers())
// clean up once the tests are done
afterAll(() => server.close())

test('displays episodes button', async () => {
    render(<EpisodeDropdown episodesLink={characterMock.results[0].episode}/>);

    const labelEpisode = "Chapters the character is featured on";
    await waitFor(() => expect(screen.getByRole('button')).toHaveTextContent(labelEpisode));
})

test('fire event show dropdown list', async () => {
    render(<EpisodeDropdown episodesLink={characterMock.results[0].episode}/>);

    const labelEpisode = "Chapters the character is featured on";
    fireEvent.click(screen.getByText(labelEpisode));

    await waitFor(() => expect(screen.getByText(episodeMock.name)).toBeInTheDocument());
})