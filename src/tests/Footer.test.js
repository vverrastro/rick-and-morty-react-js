import { render, screen } from '@testing-library/react';
import Footer from '../components/Footer';

test('renders author name in footer', () => {
	render(<Footer />);
	const linkElement = screen.getByText(/Vito Verrastro/i);
	expect(linkElement).toBeInTheDocument();
});