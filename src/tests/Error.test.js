import { render, screen } from '@testing-library/react';
import exceptions from "../exceptions.json";

import Error from '../components/Error';

test('checks missing partial information', () => {

    const message = "This is a partial error";

    render(<Error message={message} />);
    expect(screen.getByText(/error/i, { exact: false })).toBeInTheDocument();
});

test('checks the not found characters', () => {

    const message = "This is a not found error";

    render(<Error message={message} type={exceptions.notFound}/>);
    expect(screen.getByRole('img')).toBeInTheDocument();
});