import { render, screen } from '@testing-library/react';
import Menu from '../components/Menu';

// Configuration
import configData from "../config.json";

test('renders items in menu', () => {

	let menu = configData.menus.find(menu => {
		return menu.type == 'main';
	});
      
	render(<Menu itemsMenu={menu.items}/>);

	expect(screen.getByText(/Docs/i).closest('a')).toHaveAttribute('href', menu.items[0].link);
	expect(screen.getByText(/License/i).closest('a')).toHaveAttribute('href', menu.items[1].link);
});