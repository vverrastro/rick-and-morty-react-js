import React, { Component } from 'react';

// Configuration
import configData from "./config.json";
import exceptions from "./exceptions.json";

// Components
import Menu from './components/Menu';
import Card from './components/Card';
import Footer from './components/Footer';
import Error from './components/Error';

// Styles
import './styles/App.css';

class App extends Component {

	constructor(props) {
		super(props);
		this.state = {
			error: null,
			baseLink: configData.apiUrl
		}
	}

	state = {
		characters: null,
		totalCharacters: null,
		totalPages: null,
		next: null,
		prev: null,
		currentPage: null
	}	

	// Rickandmorty request
	makeHttpRequest = pageLink => {
		fetch(`${pageLink}`, {
			method: 'GET',
			headers: {  
			'Accept': 'application/json',
			'Content-Type': 'application/json'
			}
		})
		.then(res => res.json())
		.then(
			(data) => {
				this.setState({
					characters: data.results,
					totalCharacters: data.info.count,
					totalPages: data.info.pages,
					next: data.info.next,
					prev: data.info.prev,
					currentPage: this.calculateCurrentPage(data.info.next, data.info.prev)
				});
			},
			(error) => {
				this.setState({
					error
				});
			}
		)
	}

	// The method calculates the current page starting from next or previus one
	calculateCurrentPage = (next, prev) => {

		const regex = /page=(\d+)/;
		let page = 1;

		if (!next && prev) {
			page = parseInt(prev.match(regex)[1]) + 1;
		} else if (next && prev) {
			let nextPage = parseInt(next.match(regex)[1]);
			page = nextPage - 1;
		}
		return page;
	}

	componentDidMount() {
		this.makeHttpRequest(this.state.baseLink);
	}

	render() {

		// Main menu loading
		let menu = configData.menus.find(menu => {
			return menu.type == 'main';
		});

		const { error } = this.state;
		
		// Check if there's an error
		if (error) {

		  return (
			<div className="App">
				<header className="App-header">
					<Menu itemsMenu={menu.items} />
				</header>
				<Error message={error.message} type={exceptions.notFound}/>
				<Footer />
			</div>
		  );

		} else { 

			// Characters creation
			let characters;

			if (this.state.characters) {
				characters = this.state.characters.map(item =>(
					<Card key={item.id} character={item} />
				));
			}
	  
			// Pagination creation
			let pagination = [];
			
			if (this.state.totalPages) {
				let pages = Array.from(Array(this.state.totalPages).keys());
			  
				pagination = pages.map(number => {
		
					const classPaginagion = {
						active: "active"
					}
		
					let pageNumber = number + 1;
					let className = (this.state.currentPage && this.state.currentPage === pageNumber) ? classPaginagion.active : "";
					let link = `${this.state.baseLink}?page=${pageNumber}`;
			
					return(
						<span key={pageNumber} className={className} onClick={() => this.makeHttpRequest(link)}>{pageNumber}</span>
					);
				});
			}

			return (
				<div className="App">
					<header className="App-header">
						<Menu itemsMenu={menu.items} />
					</header>
					<div className="App-cards">
						{characters}
					</div>
					<div className="Pagination">
						{pagination}
					</div>
					<Footer />
				</div>
			);

		}
	}

}

export default App;