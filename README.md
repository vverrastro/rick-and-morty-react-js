# Rick and Morty React

It's a Front End project created using React JS in which display the profiles of the Rick & Morty characters.

![Home](src/home.png)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need

- **node from v13.12.0** _(check from the terminal using the command **node --version**)_

### Installing

How to install

1. Clone the repository in your local (git clone https://gitlab.com/vverrastro/rick-and-morty-react-js.git)
2. Installing **Node.js** from the following [page](https://nodejs.org/it/download/)

## Running the tests

- Test (from base directory) - `npm test`

## Running the project

1. Run (from base directory) - `npm start`
2. **Browser**: `http://localhost:3000`

## Resources

Following API has been used for the project.
- Rick & Morty API https://rickandmortyapi.com/documentation/#rest

## Authors

* **Vito Verrastro** - verrastro.vito90@gmail.com

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
